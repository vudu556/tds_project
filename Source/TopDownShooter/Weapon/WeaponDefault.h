// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TopDownShooter/FuncLibrary/Types.h"
#include "TopDownShooter/Weapon/ProjectileDefault.h"
#include "WeaponDefault.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);



UCLASS()
class TOPDOWNSHOOTER_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponFireStart OnWeaponFireStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(VisibleAnywhere)
		FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAdditionalWeaponInfo AdditionalWeaponInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);

	void WeaponInit();

	UFUNCTION(Server, Reliable, BlueprintCallable)
		void SetWeaponStateFire_OnServer(bool bIsFire);

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();
		UFUNCTION()
		void Fire();
	
	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShoot() const;

	//Timers
	float FireTimer = 0.0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
		float ReloadTime = 0.0f;

		//flags
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
			bool WeaponFiring = false;
		UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
			bool WeaponReloading = false;
		bool WeaponAiming = false;

		bool BlockFire = false;
		//Dispersion

		bool ShouldReduceDispersion = false;
		float CurrentDispersion = 0.0f;
		float CurrentDispersionMax = 1.0f;
		float CurrentDispersionMin = 0.1f;
		float CurrentDispersionRecoil = 0.1f;
		float CurrentDispersionReduction = 0.1f;

		//Timer Drop Magazin on Reload
		bool DropClipFlag = false;
		float DropClipTimer = -1.0;

		//Shell flag
		bool DropShellFlag = false;
		float DropShellTimer = -1.0;

		FName IdWeaponName = NAME_None;

		UPROPERTY(Replicated)
		FVector ShootEndLocation = FVector(0);

		UFUNCTION(BlueprintCallable)
			int32 GetWeaponRound();
		UFUNCTION()
		void InitReload();
		void FinishReload();
		void CancelReload();

		bool CheckCanWeaponReload();
		int8 GetAviableAmmoForReload();

		UFUNCTION(Server, Reliable)
			void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
			bool ShowDebug = false;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
			float SizeVectorToChangeShootDirecrionLogic = 100.0f;

		//
		UFUNCTION(Server, Unreliable)
			void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion);

		UFUNCTION(NetMulticast, Unreliable)
			void AnimWeaponFire_Multicast(UAnimMontage* AnimFire);
		UFUNCTION(NetMulticast, Unreliable)
			void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);
		UFUNCTION(NetMulticast, Unreliable)
			void FXWeaponFire_Multicast(UParticleSystem* FxFire, USoundBase* SoundFire);
};
