// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooter/Character/TPSCharHealthComponent.h"

void UTPSCharHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	
	float CurentDamage = ChangeValue * CoefDamage;
	if (Shield>0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield<0.0f)
		{
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTPSCharHealthComponent::ChangeHealthValue - Shield < 0"))
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
	
	
}

float UTPSCharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharHealthComponent::ChangeShieldValue(float ChangeValue)
{
	
	Shield += ChangeValue;

	//OnShieldChange.Broadcast(Shield, ChangeValue);
	ShieldChangeEvent_Multicast(Shield, ChangeValue);

	if (Shield>100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield<0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTPSCharHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);
	
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_shoeldRecoveryRateTimer);
	}
	
}

void UTPSCharHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_shoeldRecoveryRateTimer, this, &UTPSCharHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTPSCharHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_shoeldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
}

void UTPSCharHealthComponent::ShieldChangeEvent_Multicast_Implementation(float newShield, float value)
{
	OnShieldChange.Broadcast(newShield, value);
}
