// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_EnviromentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATPS_EnviromentStructure::ATPS_EnviromentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	SetReplicates(true);
}

// Called when the game starts or when spawned
void ATPS_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPS_EnviromentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMamterial = myMesh->GetMaterial(0);
		if (myMamterial)
		{
			Result = myMamterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTPS_StateEffect*> ATPS_EnviromentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_EnviromentStructure::RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
	
}

void ATPS_EnviromentStructure::AddEffect_Implementation(UTPS_StateEffect* newEffect)
{
	Effects.Add(newEffect);
	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(EffectAdd, true);
		EffectAdd = newEffect;
	}
	else
	{
		if (newEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(newEffect->ParticleEffect);
		}
	}
	
}

void ATPS_EnviromentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATPS_EnviromentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}
void ATPS_EnviromentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATPS_EnviromentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, OffsetEffect,NAME_None);
}


void ATPS_EnviromentStructure::SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = NAME_None;
			FVector Loc = OffsetEffect;
			USceneComponent* mySceneComp = GetRootComponent();
			if (mySceneComp)
			{
				UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, mySceneComp, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(newParticleSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsFinde = false;
			if (ParticleSystemEffects.Num() > 0)
			{
				while (i < ParticleSystemEffects.Num() && !bIsFinde)
				{
					if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFinde = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}
		}
		
		
	}
}





bool ATPS_EnviromentStructure::ReplicateSubobjects(UActorChannel* Chanel, FOutBunch* Bunch, FReplicationFlags* RepFlag)
{
	bool Wrote = Super::ReplicateSubobjects(Chanel, Bunch, RepFlag);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Chanel->ReplicateSubobject(Effects[i], *Bunch, *RepFlag);
		}

	}
	return Wrote;
}

void ATPS_EnviromentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPS_EnviromentStructure, Effects);
	DOREPLIFETIME(ATPS_EnviromentStructure, EffectAdd);
	DOREPLIFETIME(ATPS_EnviromentStructure, EffectRemove);
}
